# RobotSolutis

Desenvolvi um robô cujo objetivo é ficar a media distancia dos demais robôs realizando um disparo mais forte quando nessa media distancia e quando mais longe realizando um disparo mais fraco por ser um disparo que consome menos energia e ser mais rapido. Utilizei bastante o a luta entre meu robô e outros robôs disponiveis no proprio robocode para visualizar o desemprenho do robô que desenvolvi.

**Ponto Fortes**

	* O robô desenvolvido tenta fugir de robô que cheguem perto dele afim de ganhar mais bonus e causar mais dano. 
	Realizei alguns testes do meu robô contra o robô (RamFire) disponivel no programa afim de tentar desenvolver essa parte. 

	* A média distancia o robô desenvolvido causa uma quantidade consideravel de dano.

**Pontos Fracos**

	* A longa distancia causa menos dano.

	* Caso erre os tiros a media distancia ira gastar muita energia fica esgotado e vulneravel


Por fim gostei muito da atividade proposta bastante diferente de algumas entrevistas que ja realizei, foi uma experiencia realmente incrivel pois eu nunca tinha programado robos de fato. Adorei pesquisar sobre como funciona pois sou bem curioso e gostei bastante do que aprendi. Espero que venham mais atividades bacanas como essa.