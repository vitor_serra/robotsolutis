package vitorls;

import java.awt.Color;
import robocode.*;
//import java.awt.Color;

// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html
/**
 * VitorRobo - a robot by (your name here)
 */
public class VitorRobo extends Robot {

    /**
     * run: VitorRobo's default behavior
     */
    @Override
    public void run() {

        setColors(Color.red, Color.blue, Color.white);
        // Initialization of the robot should be put here

        // After trying out your robot, try uncommenting the import at the top,
        // and the next line:
        // setColors(Color.red,Color.blue,Color.green); // body,gun,radar
        // Robot main loop
        while (true) {
            // Replace the next 4 lines with any behavior you would like
            ahead(150);
            turnGunRight(360);
            back(250);
            turnGunRight(360);
            turnGunLeft(40);
        }
    }

    /**
     * onScannedRobot: What to do when you see another robot
     *
     * @param e
     */
    @Override
    public void onScannedRobot(ScannedRobotEvent e) {
        // Replace the next line with any behavior you would like
        if (e.getDistance() <= Rules.RADAR_SCAN_RADIUS / 4) {
            fire(Rules.MAX_BULLET_POWER);
            turnGunRight(0);
            turnLeft(10);
        } else {
            fire(1);
            turnGunRight(0);
            turnLeft(10);
        }

//        Rules.getBulletDamage(Rules.MAX_BULLET_POWER);
    }

    /**
     * onHitRobot: Turn to face robot, fire hard, and ram him again!
     *
     * @param e
     */
    @Override
    public void onHitRobot(HitRobotEvent e) {
        turnLeft(30);
        ahead(100);
    }

    /**
     * onHitByBullet: What to do when you're hit by a bullet
     *
     * @param e
     */
    @Override
    public void onHitByBullet(HitByBulletEvent e) {
        // Replace the next line with any behavior you would like
        turnLeft(60);
        back(20);
        turnGunRight(90);

    }

    /**
     * onHitWall: What to do when you hit a wall
     *
     * @param e
     */
    @Override
    public void onHitWall(HitWallEvent e) {
        // Replace the next line with any behavior you would like
        turnLeft(40);
        ahead(40);
        turnGunRight(90);

    }
}
